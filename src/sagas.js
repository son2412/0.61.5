import { all } from 'redux-saga/effects';
import { watchSignIn } from './Screens/SignIn/SignIn.Saga';
import { watchGetUserActive } from './Redux/Sagas/Active.Saga';
import { watchGetListGroup } from './Redux/Sagas/ListChat.Saga';
import { watchGetMyProfile } from './DrawerNavigator/MyProfile.Saga';
import { watchListChatWith } from './Redux/Sagas/ListChatWith.Saga';
import { watchSendMessage } from './Redux/Sagas/SendMessage.Saga';

export default function* rootSaga() {
  yield all([watchSignIn(), watchGetUserActive(), watchGetListGroup(), watchGetMyProfile(), watchListChatWith(), watchSendMessage()]);
}
