import { createDrawerNavigator } from '@react-navigation/drawer';
import ChatScreen from '../Screens/Chat/Chat.Screen';
import React, { Fragment } from 'react';
import colors from '../Themes/Colors';
import styles from './DrawerNavigator.Style';
import CounterScreen from '../Screens/Counter/Counter.Screen';
import { SafeAreaView } from 'react-native';
import DrawerContentScreen from './DrawerContent.Screen';

const Drawer = createDrawerNavigator();

const DrawerNavigatorScreen = () => {
  return (
    <Fragment>
      <SafeAreaView style={{ flex: 1 }}>
        <Drawer.Navigator
          drawerContent={() => <DrawerContentScreen />}
          drawerContentOptions={{
            activeTintColor: colors.primary,
            labelStyle: styles.textItemMenu
          }}>
          <Drawer.Screen name="CounterScreen" component={CounterScreen} options={{ drawerLabel: 'Counter' }} />
          <Drawer.Screen name="Chats" component={ChatScreen} options={{ drawerLabel: 'Chats' }} />
        </Drawer.Navigator>
      </SafeAreaView>
    </Fragment>
  );
};
export default DrawerNavigatorScreen;
